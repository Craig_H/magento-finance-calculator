require(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function(
        $,
        modal
    ) {
        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            buttons: [{
                text: $.mage.__('Continue'),
                class: 'mymodal1',
                click: function () {
                    this.closeModal();
                }
            }]
        };

        var $financeModal = $('.finance-modal').first();
        var popup = modal(options, $financeModal);
        $(".finance-calculator").on('click',function(){
            $financeModal.modal("openModal");
        });
    }
);
